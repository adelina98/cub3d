#ifndef CUB3D_H
# define CUB3D_H

#include "minilibx/mlx.h"
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <fcntl.h>

# define BUFFER_SIZE 1024
# define WIN_W 1024
# define WIN_H 512

typedef struct s_res
{
    int         w;
    int         h;
}              t_res;

typedef struct s_texture
{
    unsigned int	*n;
	unsigned int	*s;
	unsigned int	*e;
	unsigned int	*w;
	unsigned int	*i;
	unsigned int	c;
	unsigned int	f;
    int             er;
}              t_texture;

typedef struct  s_error
{
    int         lin;
    int         m;
    int         p;
}               t_error;

typedef struct  s_image
{
    void        *img;
    char        *ptr;
    char        *addr;
    int         bits_per_pixel;
    int         line_length;
    int         endian;
}               t_image;

typedef struct  s_position
{
    double   x;
    double   y;
    double   a;
    double   init;
    int     color;
}               t_position;

typedef struct  s_direction
{
    double   x;
    double   y;
    // double   a;
    // double   init;
    // int     color;
}               t_direction;

typedef struct s_map
{
    int s;
    int y;
    int x;
    char    **map;
}              t_map;

typedef struct s_sprite
{
    int x;
    int y;
}               t_sprite;


typedef struct s_rect
{
    int h;
    int w;
    int x;
    int y;
}              t_rect;

typedef struct  s_data
{
    void        *mlx;
    void        *win;
    t_image     image;
    t_res       res;
    t_texture   tex;
    t_error     er;
    t_map       map;
    t_position  pos;
    t_direction dir;
    t_sprite    sprite;
    
}               t_data;

int     get_next_line(int fd, char **line);
int     check_results(int bytes_to_read, char **line, char ** to_save);
int     get_result(char **line, char **to_save);
char    *to_join(int bytes_to_read, char *buf, char *to_save);
void     ft_free(char **str);
char	*ft_strchr(const char *s, int c);
char	*ft_strdup(const char *s);
char	*ft_strjoin(char const *s1, char const *s2);
int	    ft_strlen(const char *str);
int     get_map(char *file, t_data *mlx);
int     ft_map (t_data *mlx, char *line);
int   ft_resolution(t_data *mlx, char *line);
int     ft_texture(t_data *mlx, unsigned int **adr, char *line);

#endif
