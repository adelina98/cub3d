#include "../includes/cub3d.h"

t_data    *check_map_line(char *line, t_data *mlx)
{
    int i;

    i = 0;
    if (mlx->er.m != 1 && line[i] == '1') // check 1st line
    {
        while (line[i])
        {
            if (line[i] != '1' || line[i] != ' ')
                return (-1); // error - invalid map/data
            i++;
        }
        mlx->er.m = 1;
        mlx->map.y = 0;
    }
    i = 0;
    if (mlx->map.y > 0) // check other lines (not the 1st one)
    {
        while (line[i])
        {
            if (line[i] == '0' || line[i] == '1' || line[i] == ' ')
                i++;
            else if (line[i] == 'N' || line[i] == 'S'
                || line[i] == 'W' || line[i] == 'E')
                {
                    mlx->er.p++;
                }
            else if (line[i] = '2')
                {
                    mlx->map.s++;
                    i++;
                }
            else
                mlx->er.m = -1;
        }
    }
    return(mlx);
}

int     ft_map (t_data *mlx, char *line)
{
    char    **tmp;
    int     i, j;
    
    i = 1;
    mlx = check_map_line(&line[i], mlx);
    if (mlx->map.s > 1 || mlx->er.m == -1 || mlx->er.p > 1)
        return(-1); //error invalid map data s/m/p
    if (mlx->map.y > 0)
        tmp = mlx->map.map; //better to copy with memory allocation
    if (!(mlx->map.map = malloc(sizeof(char *) * (mlx->map.y + 2)))) //bc map.y = 0 - > first + NULL
        return(-1); // error memory allocation
    j = 0;
    while (j < mlx->map.y) //except for the first one
    {
        i = 0;
        if(!(mlx->map.map[j] = ft_strjoin(mlx->map.map[j], tmp[j])))
            return(-1);
        j++;
    }
    i = 0;
    ft_free(tmp);
    if (!(mlx->map.map[j] = malloc(sizeof(char) * (ft_strlen(line) + 1))))
        return(-1); // allocation memory error
    ft_strlcpy(mlx->map.map[j], line, ft_strlen(line));
    mlx->map.map[j + 1] = NULL;
    mlx->map.y++;
    return(0);
}
