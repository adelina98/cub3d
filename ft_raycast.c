#include "includes/cub3d.h"

// void    ft_angle(t_data *mlx, int i)
// {
//     mlx->angle = ((double)i - (mlx->res.w / 2)) * mlx->fov/2 / (mlx->res.w/2);
//     mlx->angle *= M_PI*2/360;

//     mlx->ray.x = mlx->dir.x * cos(mlx->angle) - mlx->dir.y * sin(mlx->angle);
//     mlx->ray.y = mlx->dir.y * cos(mlx->angle) - mlx->dir.x * sin(mlx->angle);

//     mlx->ray.x /= hypot(mlx->ray.x, mlx->ray.y);
//     mlx->ray.y /= hypot(mlx->ray.x, mlx->ray.y);
// }

void    ft_angle(t_data *mlx)
{
    mlx->cameraX = 2 * mlx->x / (double)mlx->res.w - 1;
    mlx->ray.x = mlx->dir.x + mlx->plane.x * mlx->cameraX;
    mlx->ray.y = mlx->dir.y + mlx->plane.y * mlx->cameraX;

    mlx->map.mapX = (int)mlx->pos.x;
    mlx->map.mapY = (int)mlx->pos.y;

    mlx->ray.d_d_x = (fabs)(1 / mlx->ray.x);
    mlx->ray.d_d_y = (fabs)(1 / mlx->ray.y);
}

void    ft_direction(t_data *mlx)
{
    if (mlx->ray.x < 0)
    {
        mlx->step.x = -1; //ray.v
        mlx->step.s_d_X = (mlx->pos.x - mlx->map.mapX) * mlx->ray.d_d_x;
    }
    else
    {
        mlx->step.x = 1;
        mlx->step.s_d_X = (mlx->map.mapX + 1.0 - mlx->pos.x) * mlx->ray.d_d_x;
    }
    if (mlx->ray.y < 0)
    {
        mlx->step.y = -1; // ray.w
        mlx->step.s_d_Y = (mlx->pos.y - mlx->map.mapY) * mlx->ray.d_d_y;
    }
    else
    {
        mlx->step.y = 1;
        mlx->step.s_d_Y = (mlx->map.mapY + 1.0 - mlx->pos.y) * mlx->ray.d_d_y;
    }
}

void    ft_wall_hit(t_data *mlx)
{
    int hit;

    hit = 0;
    while (hit == 0)
    {
        if (mlx->step.s_d_X < mlx->step.s_d_Y)
        {
            mlx->step.s_d_X += mlx->ray.d_d_x;
            mlx->map.mapX += mlx->step.x;
            mlx->wall_side = 0;
        }
        else
        {
            mlx->step.s_d_Y += mlx->ray.d_d_y;
            mlx->map.mapY += mlx->step.y;
            mlx->wall_side = 1;
        }
        if (mlx->map.map[mlx->map.mapX][mlx->map.mapY] == '1')
            hit = 1;
    }
}

void    ft_wall_dist(t_data *mlx)
{
    if (mlx->wall_side == 1)
    {
        if (mlx->map.mapY < mlx->pos.y)
            mlx->wall_dir = 'W';
        else
            mlx->wall_dir = 'E';
        mlx->wall_dist = (mlx->map.mapY - mlx->pos.y + (1 - mlx->step.y) / 2) / mlx->ray.y;
        mlx->wallx = mlx->pos.x + mlx->wall_dist * mlx->ray.x;
    }
    else
    {
        if (mlx->map.mapX < mlx->pos.x)
            mlx->wall_dir = 'N';
        else
            mlx->wall_dir = 'S';
        mlx->wall_dist = (mlx->map.mapX - mlx->pos.x + (1 - mlx->step.x) / 2) / mlx->ray.x;
        mlx->wallx = mlx->pos.y + mlx->wall_dist * mlx->ray.y;        
    }
    
}

void    ft_line_height(t_data *mlx)
{
    mlx->l_height = (int)(mlx->res.h / mlx->wall_dist);
    mlx->d_start = (mlx->l_height * -1) / 2 + mlx->res.h / 2;
    if (mlx->d_start < 0)
        mlx->d_start = 0;
    mlx->d_end = (mlx->l_height / 2 + mlx->res.h / 2);
    if (mlx->d_end >= mlx->res.h)
        mlx->d_end = mlx->res.h - 1;
}

int             key_press(int keycode, t_data mlx)
{
    if (keycode == 53)
        exit(0);
    return (0);
}

// void    ft_cross_ver(t_data *mlx)
// {
//     double x, y;

//     x = floor(mlx->pos.x) + mlx->step.x;
//     y = mlx->pos.y + (mlx->pos.x - x)* tan(mlx->angle);
//     while ((int)floor(y) > 0 && (int)floor(y) < mlx->map.y)
//     {
//         if (mlx->map.map[(int)floor(y)][(int)(x - 1 + mlx->step.x)] == '1')
//         {
//             mlx->hit.x = x;
//             mlx->hit.y = y;
//             mlx->hit.del = hypot(x - mlx->pos.x, y - mlx->pos.y);
//             return ;
//         }
//         x += (2 * mlx->step.x - 1);
//         y += (2 * mlx->step.x - 1) * mlx->ray.y / mlx->ray.x;
//     }
//     mlx->hit.x = mlx->pos.x;
//     mlx->hit.y = mlx->pos.y;
// }

// void    ft_cross_hor(t_data *mlx)
// {
//     double x, y;

//     y = floor(mlx->pos.y) + mlx->step.y;
//     x = mlx->pos.x + (mlx->pos.y - y)* tan(mlx->angle);
//     while ((int)floor(x) > 0 && (int)floor(y) < mlx->map.x)
//     {
//         if (mlx->map.map[(int)floor(y - 1 + mlx->step.y)][(int)floor(x)] == '1')
//         {
//             if (mlx->hit.del > hypot(x - mlx->pos.x, y - mlx->pos.y))
//             {
//             mlx->hit.x = x;
//             mlx->hit.y = y;
//             mlx->hit.del = hypot(x - mlx->pos.x, y - mlx->pos.y);
//             }
//             return ;
//         }
//         y += (2 * mlx->step.y - 1);
//         x += (2 * mlx->step.y - 1) * mlx->ray.x / mlx->ray.y;
//     }
// }

void    ft_draw_col(t_data *mlx)
{
    unsigned int color;
    int     start;
    int     c;

    color = 0xFFDAB9;
    
    while (mlx->d_start < mlx->d_end)
    {
        if (mlx->wall_side == 1)
            my_mlx_pixel_put(mlx, mlx->x, mlx->d_start, mlx->tex.n[mlx->d_start]);
        else
            my_mlx_pixel_put(mlx, mlx->x, mlx->d_start, mlx->tex.s[mlx->d_start]);
    }
    mlx_put_image_to_window(mlx->mlx, mlx->win, mlx->img, 0, 0);
}

int ray_casting(t_data *mlx)
{
    mlx->img = mlx_new_image(mlx->mlx, mlx->res.w, mlx->res.h);
    mlx->addr = (unsigned int *)mlx_get_data_addr(mlx->img, &mlx->bits_per_pixel, &mlx->line_length,
                                 &mlx->endian);
    mlx->x = 0;
    while (mlx->x < mlx->res.w)
    {
        ft_angle(mlx);
        ft_direction(mlx);
        ft_wall_hit(mlx);
        ft_wall_dist(mlx);
        ft_line_height(mlx);
        mlx->wallx -= floor(mlx->wallx);
        mlx->tex_x = mlx->wallx * (256 / 4);
        if (mlx->wall_side == 0 && mlx->dir.x > 0)
            mlx->tex_x = (256 / 4) - mlx->tex_x - 1;
        else if (mlx->wall_side == 1 && mlx->dir.y < 0)
            mlx->tex_x = (256 / 4) - mlx->tex_x - 1;
        ft_draw_col(mlx);
        mlx->x++;
    }
    mlx_hook(mlx->win, 2, 0L, key_press, mlx);
    mlx_loop(mlx->mlx);
    return(0);
}