#include "includes/cub3d.h"

void  init(t_data *mlx)
{
    mlx->mlx = mlx_init();
    mlx->res.w = 0;
    mlx->res.h = 0;
    mlx->er.lin = 0; // find error in ft_check_lines
    mlx->er.m = 0; // first line in parsing map
    mlx->er.p = 0; // NSWE on the map
    mlx->map.s = 0; // number of sprite -> cannot be more than 1
    mlx->map.y = 0; // coordinate map y = 0
    mlx->map.x = 0;
    mlx->dir.x = 0;
    mlx->dir.y = 0;
    mlx->pos.x = 0;
    mlx->pos.y = 0;
    mlx->tex.n= NULL;
    mlx->tex.s= NULL;
    mlx->tex.w= NULL;
    mlx->tex.e= NULL;
    mlx->tex.f= 0;
    mlx->tex.c= 0;
    mlx->cameraX = NULL;
    mlx->map.map = NULL;
}

int     main(int ac, char **argv)
{
    t_data  mlx;

    if (ac < 2)
        return(0); // not enough arguments // except for "--save"
    init(&mlx); // + init
    if ((get_map(argv[1], &mlx)) == -1)
        return(0); // "couldn't read a map" - error message
    
    if ((get_add_data(mlx)) == -1)
        return(-1);
    get_picture(mlx); // algorithm
    return(0);
}
