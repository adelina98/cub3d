#include "../includes/cub3d.h"

int   ft_resolution(t_data *mlx, char *line)
{
    t_res   res;
    char    **to_fill;
    int     i, j;

    if (mlx->res.w != 0 || mlx->res.h != 0)
        return (-1); // already filled
    if (!(to_fill = ft_split(line, ' ')))
        return(-1); // error - allocation of memory
    res.w = ft_atoi(to_fill[1]); // those are win_h and win_w
    res.h = ft_atoi(to_fill[2]); // ask about limits for h and w
    ft_free(to_fill);
    if (res.w <= 0 || res.h <= 0)
        return(-1); // error invalid resolution data
    return(0);
}

int  ft_texture(t_data *mlx, unsigned int **adr, char *line) // check lines at the end !!!!! line[i] != '\0'
{
    t_texture   tex;
    char        **to_fill;
    int         fd;
    void        *img;

    if (!(to_fill = ft_split(line, ' '))) // check spaces at the end (!)
        return(-1); //error - memory allocation
    // if (ft_strnstr(to_fill[1], "xpm", 3) == 0)
    //     return(-1); //error - not valid file (?)
    if (fd = open(to_fill[1], O_RDONLY) == -1)
        return (-1); //error - cannot open file
    close(fd);
    int pix_w, pix_h;
    img = mlx_xpm_file_to_image(mlx->image.ptr, to_fill[1], &pix_w, &pix_h);
    *adr = mlx_get_data_addr(img, &mlx->image.bits_per_pixel, &mlx->image.line_length,
                                 &mlx->image.endian); // save address and refer to it later
    free(img);
    return(1);
}

int  ft_floor_ceilling(unsigned int *color, char *line)
{
    int i;
    char    **to_fill;
    int     r, g, b;

    i = 1;
    while ((line[i] == ' ' || line[i] == '\t' || line[i] == '\n')
	|| (line[i] == '\r' || line[i] == '\v' || line[i] == '\f'))
        i++;
    if (!(to_fill = ft_split(&(line[i]), ',')))
        return(-1); //error - memory allocation
    r = ft_atoi(to_fill[0]);
    g = ft_atoi(to_fill[1]);
    b = ft_atoi(to_fill[2]); 
    if (r > 255 || g > 255 || b > 255
        || r < 0 || g < 0 || b < 0)
        return (-1); // error colors range -> invalid data
    *color = r*256 *256 + g*256 + b;
    return (1);
}
