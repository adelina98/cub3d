#include "../includes/cub3d.h"

int     ft_recheck_map(t_data *mlx)
{
    int i, j;

    j = 0;
    while(j < mlx->map.y)
    {
        i = 0;
        while(i < ft_strlen(mlx->map.map[j]))
        {
            if (i == 0 && mlx->map.map[j][i] != '1') // check fisrt item in each line
                return(-1); // error map
            if (i = ft_strlen(mlx->map.map[j]) - 1 && mlx->map.map[j][i] != '1') // check last item 
                return(-1); // error map
            if (j == mlx->map.y - 1 && mlx->map.map[j][i] != '1') // check last line
                return(-1); // error map
            i++;
        }
        j++;
    }
    return (0);
}

t_data    *ft_get_position(t_data *mlx)
{
    int     i, j;
    char    tmp;

    j = 0;
    while (j < mlx->map.y)
    {
        i = 0;
        while (i < ft_strlen(mlx->map.map[j]))
        {
            tmp = mlx->map.map[j][i];
            if (tmp == 'N' || tmp == 'S' || tmp == 'E' || tmp == 'W')
            {
                mlx->pos.x = (double)i;
                mlx->pos.y = (double)j;
                mlx->dir.x = (tmp == 'E' || tmp == 'W') ? 1 : 0;
                mlx->dir.x *= (tmp == 'W') ? -1 : 1;
				mlx->dir.y = (tmp == 'S' || tmp == 'N') ? 1 : 0;
				mlx->dir.y *= (tmp == 'N') ? -1 : 1;
            }
            i++;
        }
        j++;
    }
    return (mlx);
}
