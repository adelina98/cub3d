#include "../includes/cub3d.h"

int		ft_space_skip(char *line, int *i)
{
	while ((line[*i] == ' ' || line[*i] == '\t' || line[*i] == '\n')
	|| (line[*i] == '\r' || line[*i] == '\v' || line[*i] == '\f'))
		(*i)++;
	return (1);
}

int     ft_check_lines(char *line, t_data *mlx)
{
    int i;

    i = 0;
    ft_space_skip(line, &i);
    if ((line[i] == '1' || mlx->er.m == 1) && line[i] != '\0')
        mlx->er.lin = ft_map(mlx, line);
    else if (line[i] == 'R' && line[i + 1] == ' ')
        mlx->er.lin = ft_resolution(mlx, line);
    else if (line[i] == 'N' && line[i + 1] == 'O' && line[i + 2] == ' ')
        mlx->er.lin = ft_texture(mlx, &mlx->tex.n, line);
    else if (line[i] == 'S' && line[i + 1] == 'O' && line[i + 2] == ' ')
        mlx->er.lin = ft_texture(mlx, &mlx->tex.s, line);
    else if (line[i] == 'W' && line[i + 1] == 'E' && line[i + 2] == ' ')
        mlx->er.lin = ft_texture(mlx, &mlx->tex.w, line);
    else if (line[i] == 'E' && line[i + 1] == 'A' && line[i + 2] == ' ')
        mlx->er.lin = ft_texture(mlx, &mlx->tex.e, line);
    else if (line[i] == 'S' && line[i + 1] == ' ')
        mlx->er.lin = ft_texture(mlx, &mlx->tex.i, line);
    else if (line[i] == 'F' && line[i + 1] == ' ')
        mlx->er.lin = ft_floor_ceilling(&mlx->tex.f, line);
    else if (line[i] == 'C' && line[i + 1] == ' ')
        mlx->er.lin = ft_floor_ceilling(&mlx->tex.c, line);
    ft_space_skip(line, &i);
    if (line[i] != '\0')
        return(-1); // error invalid line
    return(mlx->er.lin < 0 ? -1 : 0);
}

int   get_map(char *file, t_data *mlx)
{
    int     fd;
    char    *line;
    int     ret;

    line = NULL;
    if (fd = open(file, O_RDONLY) == -1)
        return (-1); // error "cannot read a map"
    while (ret = get_next_line(fd, &line) == 1)
    {
        if (ft_check_lines(line, mlx) == -1)
            return (-1); // error 
        free(line);
    }
    close(fd);
    mlx = ft_get_position(mlx);
    mlx = ft_get_sprite(mlx); // not defined yet
    return(ft_recheck_map(mlx));
}
